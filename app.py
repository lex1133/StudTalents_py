import string

from flask import Flask, render_template, request, redirect, url_for, session
from flaskext.mysql import MySQL
from flask_hashing import Hashing
from datetime import datetime
import secrets, json

app = Flask(__name__)
app.secret_key = "QDciSSG1U3kULlwgfyHz"
mysql = MySQL()
hashing = Hashing(app)

app.config['MYSQL_DATABASE_USER'] = 'admin'
app.config['MYSQL_DATABASE_PASSWORD'] = 'ghhjkl'
app.config['MYSQL_DATABASE_DB'] = 'prof_talents'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

connect = mysql.connect()
query = connect.cursor()


@app.route('/')
def index():
    if not session.get("user") == None:
        return redirect(url_for(".user_profile"))
    else:
        return redirect(url_for(".signin"))


@app.route('/signin')
def signin():
    if not session.get("user"):
        code = session.get("login_code")
        message = None
        if not code == None:
            session.pop("login_code")
            if code == "1":
                message = {"code": code, "text": "Не верный логин или пароль!"}
        return render_template("signin.html", message=message)
    else:
        return render_template("error_page.html",
                               error_message="Пользователь уже авторизован, прежде чем авторизоваться под другим пользователем необходимо окончить текущуу сессию!")


@app.route('/signin_post', methods=['POST'])
def signin_post():
    login = request.form['login']
    password = request.form['password']
    pass_hash = hashing.hash_value(password)
    result = query.execute("SELECT * FROM `users` WHERE `username`=%s", (login))

    user_data = query.fetchone()
    if user_data:
        if pass_hash == user_data[3]:
            user = {"id": user_data[0], "login": login, "first_name": user_data[5], "second_name": user_data[6],

                    "middle_name": user_data[7],
                    "role": user_data[4]}
            session["user"] = user;
            return redirect(url_for('.index'))
        else:
            session["login_code"] = "1"
            return redirect(url_for('.signin'))
    else:
        session["login_code"] = "1"
        return redirect(url_for('.signin'))


@app.route("/profile")
def user_profile():
    if session.get("user"):
        query.execute("SELECT * FROM `directions_names`")
        directions = query.fetchall()
        query.execute("SELECT * FROM `specialty_names` ORDER BY `direction_id` DESC")
        specialty = query.fetchall()
        query.execute("SELECT * FROM `results` WHERE user_id=%s",(session.get("user")["id"]))
        last_tests = query.fetchall()
        last_test_names = {}
        for last_test in last_tests:
            query.execute("SELECT * FROM `specialty_names` WHERE id=%s",(last_test[2]))
            tmp = query.fetchone()
            last_test_names[last_test[2]] = tmp[1]
        return render_template("user_profile.html", user=session.get("user"), directions=directions,
                               specialty=specialty,
                               last_tests=last_tests,
                               last_test_names=last_test_names)
    else:
        return render_template("error_page.html", error_message="You must be authorized before visit this page")


@app.route("/check_result", methods=["GET"])
def check_result():
    id = request.values.get("id")
    if id:
        query.execute("SELECT * FROM `results` WHERE id=%s",(id))
        result = query.fetchone()
        result = json.loads(result[3])
        format_strings = ','.join(['%s'] * len(result.keys()))
        query.execute("SELECT * FROM `competencies_names` WHERE id IN (%s)" % format_strings, tuple(result.keys()))
        comp = query.fetchall()
        competencies = {}
        for com in comp:
            competencies[str(com[0])] = com[1]
        return render_template("results.html",results=result,competencies=competencies)

@app.route("/pass_test", methods=["GET", "POST"])
def pass_test():
    if session.get("user"):
        check_test = request.form.get("check_test")
        if check_test:
            competencies = {}
            for answer in request.form:
                if not answer == "check_test":
                    query.execute("SELECT * FROM `answers` WHERE id=%s", (answer))
                    answer = query.fetchone()
                    if not ',' in answer[3]:
                        if not answer[3] == "":
                            if competencies.get(answer[3]) == None:
                                competencies[answer[3]] = int(answer[4])
                            else:
                                competencies[answer[3]] += int(answer[4])
                        else:
                            continue
                    else:
                        comp_split = answer[3].split(",")
                        answ_split = answer[4].split(",")
                        i = 0
                        for comp in comp_split:
                            if competencies.get(comp) == None:
                                competencies[comp] = int(answ_split[i])
                            else:
                                competencies[comp] += int(answ_split[i])
                            i += 1
            query.execute("INSERT INTO `results`  (`id`, `user_id`, `test_id`, `answers`) VALUES(NULL,%s,%s,%s)", (session.get("user")["id"],request.values.get("prof"), json.dumps(competencies)))
            connect.commit()
            return redirect(url_for(".user_profile"))

        prof = request.values.get("prof")
        if prof:
            query.execute("SELECT * FROM `questions` WHERE specialty_id=%s", (prof))
            questions = query.fetchall()
            answers = {}
            for question in questions:
                if answers.get(question[0]) == None:
                    answers[question[0]] = []
                    query.execute("SELECT * FROM `answers` WHERE question_id=%s", (question[0]))
                    answers_query = query.fetchall();
                    for answer_q in answers_query:
                        answers[question[0]].append([answer_q[0], answer_q[2], answer_q[3], answer_q[4]])
            return render_template("pass_test.html", questions=questions, answers=answers)
        else:
            return "asdasdas"
    else:
        return render_template("error_page.html", error_message="You must be authorized before visit this page")


@app.route("/tests", methods=["GET", "POST"])
def test():
    user = session.get("user")
    if user:
        query.execute("SELECT * FROM `users` WHERE username=%s", (user["login"]))
        user_data = query.fetchone()
        role = user_data[4]
        query.execute("SELECT role_description FROM `roles` WHERE id=%s", (role))
        role_text = query.fetchone()
        if int(role) in [1, 3]:
            add_answer = request.form.get("add_answer")
            if add_answer:
                question = request.form.get("question")
                competencies = ""
                points = ""
                for value in request.form:
                    if "points-" in value:
                        if not request.form[value] == "":
                            competencies += value[7:] + ","
                            points += request.form[value] + ","

                query.execute("INSERT INTO `answers` (`id`, `question_id`, `answer_text`, `competence_id`, `competence_points`) VALUES(NULL,%s,%s,%s,%s)",
                              (question, add_answer, competencies[:-1], points[:-1]))
                connect.commit()
            add_question = request.form.get("add_question")
            if add_question:
                query.execute("INSERT INTO `questions`  (`id`, `specialty_id`, `wording`, `type_id`) VALUES (NULL,%s,%s,%s)", (
                    request.form.get("specialty"), request.form.get("add_question"), request.form.get("type")))
                connect.commit()
            quest_del = request.form.get("quest_del")
            if quest_del:
                query.execute("DELETE FROM `questions` WHERE id=%s", (quest_del))
                connect.commit()
            answ_del = request.form.get("answ_del")
            if answ_del:
                query.execute("DELETE FROM `answers` WHERE id=%s", (answ_del))
                connect.commit()
            id = request.values.get("id")
            q_id = request.values.get("q_id")
            if id == None and q_id == None:
                query.execute("SELECT * FROM `specialty_names`")
                specialty = query.fetchall()
                return render_template("tests_panel.html", specialties=specialty)
            elif q_id:
                query.execute("SELECT * FROM `questions` WHERE id=%s", (q_id))
                question = query.fetchone()
                query.execute("SELECT * FROM `answers` WHERE question_id=%s", (q_id))
                answers = query.fetchall()
                query.execute("SELECT * FROM `competencies_names` WHERE specialty_id=%s", question[1])
                competencies = query.fetchall()
                return render_template("answers_panel.html", question=question, answers=answers,
                                       competencies=competencies)
            else:
                query.execute("SELECT * FROM `questions` WHERE specialty_id=%s", (id))
                questions = query.fetchall()
                query.execute("SELECT * FROM `questions_types`")
                types = query.fetchall()
                return render_template("questions_panel.html", questions=questions, types=types, specialty=id)
        else:
            return render_template("error_page.html", error_message="You haven't permissions for visiting this page")
    else:
        return render_template("error_page.html", error_message="You must be logined")


@app.route("/directions", methods=["POST", "GET"])
def direction():
    user = session.get("user")
    if user:
        query.execute("SELECT * FROM `users` WHERE username=%s", (user["login"]))
        user_data = query.fetchone()
        role = user_data[4]
        query.execute("SELECT role_description FROM `roles` WHERE id=%s", (role))
        role_text = query.fetchone()
        if int(role) in [1, 3]:
            cur_dir = request.form.get("direction")

            direction_add = request.form.get("direction_add")
            if not direction_add == None:
                query.execute("INSERT INTO `directions_names` VALUES ('',%s)", (direction_add))
                connect.commit()

            del_spec = request.form.get("del_spec")
            if not del_spec == None:
                query.execute("DELETE FROM `specialty_names` WHERE id=%s", (del_spec))
                connect.commit()

            del_dir_id = request.form.get("del_dir_id")
            if not del_dir_id == None:
                query.execute("DELETE FROM `specialty_names` WHERE direction_id=%s", (del_dir_id))
                connect.commit()
                query.execute("DELETE FROM `directions_names` WHERE id=%s", (del_dir_id))
                connect.commit()

            speciality_add = request.form.get("speciality_add")
            speciality_add_dir_id = request.form.get("speciality_add_dir_id")
            if not speciality_add == None:
                cur_dir = speciality_add_dir_id
                query.execute("INSERT INTO `specialty_names` VALUES ('',%s,%s)",
                              (speciality_add, speciality_add_dir_id))
                connect.commit()

            specialty = None
            if not cur_dir == None:
                cur_dir = int(cur_dir)
                query.execute("SELECT * FROM `specialty_names` WHERE direction_id=%s", (cur_dir))
                specialty = query.fetchall()
            query.execute("SELECT * FROM `directions_names`")
            directions = query.fetchall()
            return render_template("directions_panel.html", directions=directions, cur_dir=cur_dir, specialty=specialty)
        else:
            return render_template("error_page.html", error_message="You haven't permissions for visiting this page")
    else:
        return render_template("error_page.html", error_message="You must be logined")


@app.route("/log_out")
def logout():
    session.pop("user");
    return redirect(url_for(".index"))


@app.route("/competence", methods=["POST", "GET"])
def competence():
    id = request.values.get("id")
    if id:
        add_comp = request.values.get("add_comp")
        if add_comp:
            query.execute("INSERT INTO `competencies_names` (`id`, `competence_description`, `specialty_id`) VALUES (NULL,%s,%s)", (add_comp, id))
            connect.commit()
        query.execute("SELECT * FROM `specialty_names` WHERE id=%s", (id))
        specialty = query.fetchone()
        query.execute("SELECT * FROM `competencies_names` WHERE specialty_id=%s", (id))
        competencies = query.fetchall()

        return render_template("speciality_panel.html", specialty=specialty, competencies=competencies)


@app.route("/tickets")
def ticket_panel():
    user = session.get("user")
    if user:
        query.execute("SELECT * FROM `users` WHERE username=%s", (user["login"]))
        user_data = query.fetchone()
        role = user_data[1]
        query.execute("SELECT name FROM `roles` WHERE id=%s", (role))
        role_text = query.fetchone()
        if int(role) in [1, 3]:
            query.execute("SELECT * FROM `tickets`")
            tickets = query.fetchall()
            return render_template("ticket_panel.html", user=user, role=role_text, tickets=tickets)
        else:
            return render_template("error_page.html", error_message="You haven't permissions for visiting this page")
    else:
        return render_template("error_page.html", error_message="You must be logined")


@app.route("/ticket_post", methods=["POST"])
def ticket_post():
    if not request.form.get("agree") == None:
        query.execute("SELECT * FROM `tickets` WHERE id=%s", (request.form.get("id")))
        login = "user_" + ''.join(secrets.choice(string.ascii_letters + string.digits) for i in range(10))
        ticket = query.fetchone();
        query.execute("INSERT INTO `users` VALUES('','',%s,%s,%s,%s,%s,%s,%s)",
                      (login, "", ticket[1], ticket[2], ticket[3], ticket[8], ticket[4]))
        connect.commit()
        query.execute("DELETE FROM `tickets` WHERE id=%s", (request.form.get("id")))
        connect.commit()
        return redirect(url_for(".ticket_panel"))
    else:
        query.execute("DELETE FROM `tickets` WHERE id=%s", (request.form.get("id")))
        connect.commit()
        return redirect(url_for(".ticket_panel"))


@app.route('/signup')
def signup():
    code = session.get("register_code")
    message = None
    if not code == None:
        session.pop("register_code")
        if code == "1":
            message = {"code": code, "text": "Одно из ключевых полей не заполненно!"}
        elif code == "2":
            message = {"code": code, "text": "Не принято соглашение об использовании персональных данных!"}
        elif code == "3":
            message = {"code": code, "text": "Пользователь с данным E-mail уже существует!"}
        elif code == "4":
            message = {"code": code,
                       "text": "Заявка с таким E-mail уже существует. Необходимо дождаться одобрения регистрации!"}
        elif code == "5":
            message = {"code": code,
                       "text": "Заявку могут подять только лица достигшие 14 лет!"}
        elif code == "0":
            message = {"code": code, "text": "Заявка принята в обработку"}
    return render_template("signup.html", message=message)


@app.route('/signup_post', methods=['POST'])
def signup_post():
    fist_name = request.form.get('first_name')
    second_name = request.form.get('second_name')
    middle_name = request.form.get('middle_name')
    email = request.form.get('email')
    birthday = request.form.get('birthday')
    institution = request.form.get('institution')
    go_to_stankin = request.form.get('go_to_stankin')
    personal_information = request.form.get('personal_information')
    if fist_name == None or second_name == None or email == None or birthday == None or institution == None:
        session["register_code"] = "1"
        return redirect(url_for('.signup'))
    elif personal_information == None:
        session["register_code"] = "2"
        return redirect(url_for('.signup'))
    else:
        date_delta = (datetime.now().date() - datetime.strptime(birthday, "%Y-%m-%d").date()).days / 365.25;
        if date_delta < 14:
            session["register_code"] = "5"
            return redirect(url_for('.signup'))
        query.execute("SELECT * FROM `users` WHERE email=%s", (email))
        if query.fetchone():
            session["register_code"] = "3"
            return redirect(url_for('.signup'))
        query.execute("SELECT * FROM `tickets` WHERE email=%s", (email))
        if query.fetchone():
            session["register_code"] = "4"
            return redirect(url_for('.signup'))
        personal_information = personal_information = "on" and 1 or 0
        go_to_stankin = go_to_stankin = "on" and 1 or 0
        result = query.execute("INSERT INTO `tickets` VALUES ('', %s, %s, %s, %s, %s, %s, %s, %s, %s)", (
            fist_name, second_name, middle_name, birthday, institution, go_to_stankin, personal_information, email,
            datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        connect.commit();
        if result:
            session["register_code"] = "0"
            return redirect(url_for('.signup'))


if __name__ == '__main__':
    app.run()
