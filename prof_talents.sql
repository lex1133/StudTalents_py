-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 20 2019 г., 05:48
-- Версия сервера: 5.7.26-0ubuntu0.18.04.1
-- Версия PHP: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `prof_talents`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `question_id` int(5) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `competence_id` varchar(30) NOT NULL,
  `competence_points` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer_text`, `competence_id`, `competence_points`) VALUES
(3, 1, 'Что я в основном сам решаю, что и как мне делать', '6', '1'),
(4, 1, 'Что она дает мне возможность проявить то, что я знаю и умею.', '4', '1'),
(5, 1, 'Что я чувствую себя полезным и нужным.', '5', '1'),
(6, 1, 'Что мне за нее относительно неплохо платят.', '3', '1'),
(7, 1, 'Особенно ничего не ценю, но эта работа мне хорошо знакома и привычна.', '2', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `competencies_names`
--

CREATE TABLE `competencies_names` (
  `id` int(5) NOT NULL,
  `competence_description` varchar(255) NOT NULL,
  `specialty_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `competencies_names`
--

INSERT INTO `competencies_names` (`id`, `competence_description`, `specialty_id`) VALUES
(2, 'Люмпенизированный тип', '1'),
(3, 'Инструментальный тип', '1'),
(4, 'Профессиональный тип', '1'),
(5, 'Патриотический тип', '1'),
(6, 'ХО  - хозяйский тип', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `directions_names`
--

CREATE TABLE `directions_names` (
  `id` int(5) NOT NULL,
  `area_desription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `directions_names`
--

INSERT INTO `directions_names` (`id`, `area_desription`) VALUES
(1, 'Общие тесты');

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int(5) NOT NULL,
  `specialty_id` int(5) NOT NULL,
  `wording` varchar(1500) NOT NULL,
  `type_id` int(5) NOT NULL,
  `test_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `specialty_id`, `wording`, `type_id`, `test_id`) VALUES
(1, 1, 'Что Вы больше всего цените в своей работе? Можете дать один или два варианта  ответа:', 2, NULL),
(2, 1, 'Какое выражение из перечисленных ниже Вам подходит более всего? Дайте только  один ответ', 1, NULL),
(3, 1, 'Как Вы предпочитаете работать? Можете дать один или два варианта ответа:', 2, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `questions_types`
--

CREATE TABLE `questions_types` (
  `id` int(5) NOT NULL,
  `type_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `questions_types`
--

INSERT INTO `questions_types` (`id`, `type_description`) VALUES
(1, 'Один из множества'),
(2, 'Множество из множества'),
(3, 'Произвольный ответ');

-- --------------------------------------------------------

--
-- Структура таблицы `registration_validation`
--

CREATE TABLE `registration_validation` (
  `id` int(5) NOT NULL,
  `fisrt_name` varchar(100) NOT NULL,
  `second_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `current_institution` varchar(255) NOT NULL,
  `go_to_stankin` binary(1) NOT NULL DEFAULT '0',
  `consent_to_processing` binary(1) NOT NULL DEFAULT '0',
  `application_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `results`
--

CREATE TABLE `results` (
  `id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `test_id` int(5) NOT NULL,
  `answers` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(5) NOT NULL,
  `role_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `role_description`) VALUES
(1, 'Администратор');

-- --------------------------------------------------------

--
-- Структура таблицы `specialty_names`
--

CREATE TABLE `specialty_names` (
  `id` int(11) NOT NULL,
  `specialty_description` varchar(255) NOT NULL,
  `direction_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `specialty_names`
--

INSERT INTO `specialty_names` (`id`, `specialty_description`, `direction_id`) VALUES
(1, 'Тест на определение мотивации', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(5) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role` int(5) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `second_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`, `first_name`, `second_name`, `middle_name`, `birthday`) VALUES
(1, 'admin', 'stud115089@stankin.ru', 'f780c751a3e06023942854d48973c167a3053cfc550c8cc97a13236f79530b3d', 1, 'Алексей', 'Данилин', 'Олегович', '1997-06-16');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Индексы таблицы `competencies_names`
--
ALTER TABLE `competencies_names`
  ADD PRIMARY KEY (`id`),
  ADD KEY `specialty_id` (`specialty_id`);

--
-- Индексы таблицы `directions_names`
--
ALTER TABLE `directions_names`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `specialty_id` (`specialty_id`),
  ADD KEY `test_id_2` (`test_id`);

--
-- Индексы таблицы `questions_types`
--
ALTER TABLE `questions_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `registration_validation`
--
ALTER TABLE `registration_validation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `results_ibfk_2` (`test_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `specialty_names`
--
ALTER TABLE `specialty_names`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direction_id` (`direction_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `competencies_names`
--
ALTER TABLE `competencies_names`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `directions_names`
--
ALTER TABLE `directions_names`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `questions_types`
--
ALTER TABLE `questions_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `registration_validation`
--
ALTER TABLE `registration_validation`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `results`
--
ALTER TABLE `results`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `specialty_names`
--
ALTER TABLE `specialty_names`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);

--
-- Ограничения внешнего ключа таблицы `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`),
  ADD CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `questions_types` (`id`),
  ADD CONSTRAINT `questions_ibfk_3` FOREIGN KEY (`specialty_id`) REFERENCES `specialty_names` (`id`);

--
-- Ограничения внешнего ключа таблицы `results`
--
ALTER TABLE `results`
  ADD CONSTRAINT `results_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `results_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `specialty_names` (`id`);

--
-- Ограничения внешнего ключа таблицы `specialty_names`
--
ALTER TABLE `specialty_names`
  ADD CONSTRAINT `specialty_names_ibfk_1` FOREIGN KEY (`direction_id`) REFERENCES `directions_names` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
